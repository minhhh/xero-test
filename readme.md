Beanworks Xero Integration
==========================

This is solution designed to extract both the list of Accounts, and the list of Vendors, from a demo sample company, and store them on disk. The system is called **Xero Extractor**

A HTML version of this file is available at [readme.html](./readme.html)

Business requirements
---------------------

When designing this solution, there are some assumptions that I made, which are detailed below

▼ Assumptions

* The list of Accounts and the list of Vendors are to be stored separately. It could be easier if later we add more data points from Xero, such as invoices, then we can quickly process the data points by filename.
* We are using Xero's Oauth2 APIs, because there seems to be no choice to choose Oauth1 when creating account.
* The client has an interface to manually grant access to Beanworks using Xero's Oauth2 flow.
* Beanworks has a datastore to store access and refresh tokens from Xero, and a service to refresh access tokens. The reason for this is we need a centralized place to secure refresh tokens due to their fragile nature.


**Xero issue**

* Basically, the way Oauth2 refresh token is being implemented with Xero right now is that the old refresh token is invalidated after refresh request and a new refresh token is issued along with a new access token
* This is a serious issue because now we have to make sure that we store working refresh token in the Beanworks system somewhere.
    * The reasons why we don't handout refresh token casually to other services (so that they can just use the refresh token themselves) is because
        1. If the service goes down after making a refresh token request, we lose the new refresh token and the customer will have to reauthorize Beanworks on Xero
        2. If 2 services are making refresh token at the same time or the request times are close to each other, and suppose they can update the new refresh token back to the Token store successfuly, we have a race condition where the newer refresh token might be lost.
* Consequently, we don't handout refresh token, but manage it in a centralized place. The refresh token will be refreshed only at that centralized place

More information about the refresh token issue with Xero is here [https://community.xero.com/developer/discussion/105714701](https://community.xero.com/developer/discussion/105714701)

* They're fixing it. Looks like they just added (on Jan 30 2020) a grace period of 30 minutes , where you can still use the old refresh token. It's just a work-around, and it does not change our solution
* It is a serious blocking issue for some people to move to Oauth2
* Many people suggest immortal refresh token


High-level flow
===============

**Xero Extractor** is a stand-alone web service designed to be ran internally and aside Invoice System. It has an API (no user interface) that other system can call to request extraction of data points from Xero system.

Below is a high-level interaction flow of one way that Xero Extractor can be integrated in Beanworks system.

![](./imgs/high-level-flow.png)


### Explanation of each steps

▼ Pre main flow

1. Client is expected to authorize Beanworks for Xero access using an entry point from Beanworks system or website
2. After the client authorizes Beanworks, Beanworks can get a refresh token and an access token from Xero. Step 1 and 2 are actually more back and forth than represented here and they follow Oauth2 authentication and authorization flow.
3. Beanworks system will store the tokens for the corresponding client id in a Token store where other services can access

▼ Main flow

4. When a mail from the client arrives at the Invoice System, it will trigger the approval process.
5. Invoice System recognizes that the mail is from a client with Xero, so it retrieves Xero's access token and client id from the Token Store. Note that the Token store is not just a database, it will be in charge of refreshing the access and refresh tokens so that the access token is always valid when being handed out to Invoice System
6. Invoice System sends a request to Xero Extractor, along with the client id and access token.
7. Xero Extractor extracts the necessary data points from Xero, in this case Accounts and Vendor list
8. Xero Extractor writes to disk, in this case, we assume a Shared NFS
9. Invoice System retrieves the files and parses them accordingly.

▼ Extra flow

10. Maybe there is a Notification System or Message Queue somewhere so Xero Extractor can push `created` events to it.
11. Some other service, for example File Archiver will pick up the events
12. File Archiver will pick up the newly generated files
13. File Archiver will push them to Amazon S3


API
---

### Extract

▼ Overview

|        |            |   |
| ------------- |--------------|
| **URL**      | https://x.x.x.x/extract |
| **Methods supported**      | POST      |
| **Body** | • xero_tenant_id: The tenant id we get from https://api.xero.com/connections <br> • access_token: A working access token to the tenant   |
| **Response** | JSON data: { uris: [\<filePaths\>] }, where filePaths are the path of the written files on disk  |


▼ File name

The format of the file names is: `xero-<datapointname>.<tenant-id>.<ISO 8601 format>.json`.

Example file names: `xero-accounts.3fdb10ce-66d7-4ef4-b494-c18ac4efe084.20200130T051550829Z.json`, `xero-contacts.3fdb10ce-66d7-4ef4-b494-c18ac4efe084.20200130T051025119Z.json`


▼ Error code

In case of error, the service will return HTTP code 4xx or 5xx, with the following JSON data in the body:

```
{
    status: <http_status>,
    errorCode: <application-specific error code>
    data: <error data>
}
```

* HTTP 400
    * Incorrect inputs from user, e.g. incorrect access token
        * `errorCode`: 2
        * `data`: Error data from Xero

* HTTP 500
    * Timeout
        * `errorCode`: 1
        * `data`: "Timeout"
    * Exceptions and other errors
        * `errorCode`: 0
        * `data`: message or error stack


### Health

▼ Overview

|        |            |   |
| ------------- |--------------|
| **URL**      | https://x.x.x.x/health |
| **Methods supported**      | GET      |
| **Body** | NONE   |
| **Response** | Status 200 |


This endpoint is for health checking. It does not check the service true health (yet).


Setup Instructions
==================

The setup was tested on MacOS, but it should work on Linux,Windows with some slight modifications.

Local development
-----------------

▼ Install requirements

* Install `node 10.18.1`
* Install `yarn` globally

```
npm install -g yarn
```

▼ Run the app in dev mode

```
npm run start:dev
```

Then the app will be available at `http://localhost:3000`. It uses `nodemon` so that any changes in the source code will be detected and the server will be restarted on change.


The config
----------

The configuration for our app is stored in `xero-extractor/src/config.js`. It will have the following parameters:

* `NODE_ENV`: `production` or `development`
* `PORT`: The port to be listened on. In dev, it's 3000
* `CLIENT_ID`: Client id from Xero. This is the client id from the app we created on Xero (see https://github.com/XeroAPI/xero-node#readme)
* `CLIENT_SECRET`: Client secret from Xero, see https://github.com/XeroAPI/xero-node#readme about how to get it
* `XERO_API_ENDPOINT_V2`: "https://api.xero.com/api.xro/2.0"
* `LOG_DIR`: The directory where we store logs from our application
* `COMBINED_LOG_FILENAME`: The name of the file where all logs will go to
* `ERROR_LOG_FILENAME`: The nameof the file where only error logs go to
* `OUTPUT_DIR`: The path to the folder where we store the data points extracted from Xero

The `config.js` is just a symlink to the real config file. In development mode it is symlinked to `xero-extractor/src/config.dev.js`. In production mode, you will have to somehow link the real production configuration file here.

In order to test, you have to create your own app from Xero portal, using current values in the config file might not work.


Using conda
-----------
Normally, getting the exact node version on your system can be a problem. If you try to use system's package manager, such as `apt` on Ubuntu, it's hard to get the exact version. Secondly, the global node environment will be polluted with many global packages and it can cause conflict.

One solution is to use node version manager tools such as `nvm` or `n`. However, even these tools are not good enough because even though you can get the exact node version, you still have the issue of global environment pollution. Python `virtualenv` style is a good solution to deal with this.

In fact, we have the option use Python tool `conda` and `nodeenv` Python package to setup our development environment. First, install `conda` following the instruction here: [https://docs.conda.io/projects/conda/en/latest/user-guide/install/](https://docs.conda.io/projects/conda/en/latest/user-guide/install/), any version is fine.

Setup `conda` environment

```
cd xero-extractor
conda env create -f condaenv.yml
```

After this, a virtual Conda environment will be created with the name `xero-test`. This environment is contained in a folder and does not affect your system's Python. In this environment, we already have [https://github.com/ekalinin/nodeenv](https://github.com/ekalinin/nodeenv), which is a Python tool to manage virtual environments for Node.js

Activate `conda` environment

```
conda activate xero-test
```

Install `node` to the `conda` virtual environment

```
nodeenv --node=10.18.1 -p
```

After this, you have `node` 10.18.1 installed into the self-contained `conda` environment and it does not affect your system's `python` nor `node`. Everytime you want to use this environment you will run `conda activate xero-test`. Then you can follow the instructions above to run the app.


Using docker for dev
--------------------

Using `conda` is ok for `node` alone, but if our app requires more dependencies, it will be problematic to install directly on host computer. A better choice is to use containers.

First install `docker` following this instruction (https://docs.docker.com/install/)

Then use `docker-compose` to build our service and bring it up

```
docker-compose -f docker-compose.dev.yml --build
docker-compose -f docker-compose.dev.yml up
```

The new container will expose port 3000 to the host. The source code is shared with the container so we can continue to modify our source code on the host and the changes will be reflected in the docker container


**Note**: It's better to either develop on host or on Docker environment, but not both. The reason is the `node_modules` folder is shared between host and container, but the packages inside of it will be different when running `npm install` or `yarn install` on host vs on docker container, so the packages might be easy to be messed up after a while.




Testing
-------

### Linting

This project use `eslint` for static analysis.

To run eslint:

```
npm run lint
```

Modify `.eslintrc.json` if needed.

### Unit tests

Unit tests are in `xero-extractor/tests/unit`. They use `mocha` with `chai` for BDD style tests.

The test tokens are stored in `xero-extractor/tests/unit/tokens.json` and the test config is stored in `xero-extractor/tests/unit/config.js`

Everytime the tests are ran, we refresh tokens and write them back to `xero-extractor/tests/unit/tokens.json` so that it can still be used next time.

To get the initial set of tokens for the first time, you can use the sample app [https://github.com/XeroAPI/xero-node-oauth2-app](https://github.com/XeroAPI/xero-node-oauth2-app), then update the property `refresh_token` in `xero-extractor/tests/unit/tokens.json` and `XERO_TENANT_ID` in `xero-extractor/tests/unit/config.js`


### Containerized testing
Containerized testing will minimize the burden of setup on host, and even though it's a little bit slower (due to the container start time), it's still pretty fast.

To test our app on Docker container, we have a docker compose file `docker-compose.test.yml`, which is the same as `docker-compose.dev.yml`, except the final command is `npm test` instead of `npm run start:dev`

To run unit test on container:

```
docker-compose -f docker-compose.test.yml build # run once only

docker-compose -f docker-compose.test.yml run --rm web
```

### Integration tests

Integration tests haven't been implemented yet.

One option is to use tool like `postman`. It can also be a good way to document the API. One example is given in `xero-extractor/tests/integration/xero-test.postman_collection.json`


Running on production
---------------------

On production, we should run the following command

```
NODE_ENV=production node dist/index.js
```

Remember to change the `dist/config.js` accordingly for production environment

Since `node` is single thread, we should use another process manager which will help spawning multiple node process to utilize the multi cores of the CPU and also make sure the process is restarted if needed, for instance, `pm2`, `forever`

There also a `Dockerfile` if we want to deploy the app on contaners. It has a simple health check to be used with other ocherstration tools.


Application design and consideration
====================================

* Output file
    * I decided to save the whole response from Xero, not just the array of Accounts or Contacts, because the other parts of the response might have useful information for later use.

* Why web service
    * For a one-off extraction task we have 2 choices: 1. Make it a short-running task, or 2. Make it a long-running task constantly expecting an event to do its job
    * Either way is fine. If it's a short-running task, we can deploy it on something like Amazon Lambda. If it's a long running task, we deploy it on Amazon ECS.
    * I haven't used the Amazon services so I don't know if one is more expensive or advantageous over the other. Maybe Lambda will take a little bit longer to run due to the cold-start, but it doesn't seem that we need near real-time response in the case of Xero Extractor

* Api rate limiting: It could be done inside our service, but can also be done outside at the API gateway or something. Since this is an internal service with a limited usecase only for Xero, letting Xero API themselves handle rate limit is Ok.

* No authentication, authorization, relaxed network security because the service is used internally.

* The service won't try to spawn multiple processes to utilize CPU cores because it is assumed that some external manager will do that, either at host level or container level.


THINGS THAT CAN BE IMPROVED
===========================

* Architecture
    * Maybe using a message queue for these type of application is better.

* Output file
    * Maybe add a way to choose output target, i.e. Amazon S3.
    * Add version to output file so that later on we can change its format and be able to still parse the old files.

* Logging
    * There should be some way to generate uuid for each request and attach it to the log so we can track request flow
    * We should log to a log server, or a log process should watch the log files and pick up generated log lines.
    * Fatal log, crash logs, and crashes themselves should be sent to a error reporting service somehow. There should be something that monitors the application performance

* Health check
    * Should really check if the service can function properly, i.e. check its dependencies

* API
    * Should verify parameters and send back more sensible errors
    * Maybe should have a way to choose response format based on Accept header

* Project structure could be improved
    * When the project gets more features, we should switch to specific style, where we have separate folder for controllers, views, models, etc.

* Tests
    * Tests should show coverage percentage
    * Tests should not be fragile. We should have a dedicated test company just for tests instead of using the demo company because the data in Demo Company can be changed by Xero anytime
    * Even so, there can still be problems with many tests running against the test company if some of them have side effects such as tests for update, deletion, addition


REFERENCES
==========

* Options to test organizations: https://developer.xero.com/documentation/getting-started/development-accounts
* Oauth2 flow: https://developer.xero.com/documentation/oauth2/auth-flow
