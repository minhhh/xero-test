const config = require("./config");
const winston = require("winston");
const expressWinston = require("express-winston");
const hostname = require("os").hostname();
const ipAddress = require("ip").address();
const path = require("path");

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: {
    host: hostname,
    ip: ipAddress,
    timestamp: Date.now(),
    pid: process.pid
  },
  transports: [
    new winston.transports.File({
      filename: path.join(config.LOG_DIR, config.ERROR_LOG_FILENAME),
      level: "error"
    }),
    new winston.transports.File({
      filename: path.join(config.LOG_DIR, config.COMBINED_LOG_FILENAME)
    })
  ]
});

if (config.NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
      level: "debug"
    })
  );
}

var expressWinstonLogTransports = [
  new winston.transports.File({
    filename: path.join(config.LOG_DIR, config.COMBINED_LOG_FILENAME)
  })
];
var expressWinstonErrorLogTransports = [
  new winston.transports.File({
    filename: path.join(config.LOG_DIR, config.ERROR_LOG_FILENAME)
  })
];

if (config.NODE_ENV !== "production") {
  expressWinstonLogTransports.push(
    new winston.transports.Console({
      format: winston.format.simple()
    })
  );
  expressWinstonErrorLogTransports.push(
    new winston.transports.Console({
      format: winston.format.simple()
    })
  );
}

const expressWinstonLogger = expressWinston.logger({
  transports: expressWinstonLogTransports,
  format: winston.format.json(),
  baseMeta: {
    host: hostname,
    ip: ipAddress,
    timestamp: Date.now(),
    pid: process.pid
  },
  meta: true,
  msg: "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
  expressFormat: false,
  colorize: false,
  ignoreRoute: function(req, res) {
    return false;
  }
});

const expressWinstonErrorLogger = expressWinston.errorLogger({
  transports: expressWinstonErrorLogTransports,
  format: winston.format.json()
});

module.exports = {
  logger: logger,
  expressWinstonLogger: expressWinstonLogger,
  expressWinstonErrorLogger: expressWinstonErrorLogger
};
