#!/usr/bin/env node

const app = require("./server");
const debug = require("debug")("web");
const http = require("http");
const config = require("./config");
const PORT = config.PORT;

app.set("port", PORT);

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(PORT);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + PORT : "Port " + PORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(
        new Date().toUTCString() +
          " HTTP error: " +
          bind +
          " requires elevated privileges"
      );
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(
        new Date().toUTCString() + " HTTP error: " + bind + " is already in use"
      );
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Express server started at port :" + config.PORT);
}

process.on("uncaughtException", handleUncaughtException);

function handleUncaughtException(err) {
  console.error(new Date().toUTCString() + " uncaughtException:", err.message);
  console.error(err.stack);
  process.exit(1);
}
