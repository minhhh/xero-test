const config = require("./config");
const debug = require("debug")("web");
const express = require("express");
const createError = require("http-errors");
const path = require("path");
const bodyparser = require("body-parser");
const logger = require("./logger").logger;
const expressWinstonLogger = require("./logger").expressWinstonLogger;
const expressWinstonErrorLogger = require("./logger").expressWinstonErrorLogger;
const extractController = require("./ExtractController");

debug("Start creating app instance");

var app = express();

app.use(expressWinstonLogger);

app.use(
  bodyparser.urlencoded({
    extended: true
  })
);
app.use(bodyparser.json());
app.use("/extract", extractController);
app.get("/health", (req, res) => {
  res.send();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(expressWinstonErrorLogger);

// error handler
app.use(function(err, req, res, next) {
  const status = (err && err.status) || 500;

  res.status(status || 500);
  res.json(err);
});

module.exports = app;
