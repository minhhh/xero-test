const config = require("./config");
const express = require("express");
const router = express.Router();
const logger = require("./logger").logger;
const axios = require("axios");
const utils = require("./utils");
const fs = require("fs").promises;
const path = require("path");

router.post("/", (req, res, next) => {
  const { access_token, xero_tenant_id } = req.body;
  var fileNames = [];
  // logger.debug("========req.body " + JSON.stringify (req.body));

  axios({
    method: "get",
    url: "Contacts",
    baseURL: config.XERO_API_ENDPOINT_V2,
    headers: {
      Authorization: "Bearer " + access_token,
      Accept: "application/json",
      "Xero-tenant-id": xero_tenant_id
    }
  })
    .then(response => {
      // logger.debug("========response " + JSON.stringify(response.data));
      // logger.debug("========time " + utils.getISOString());
      var fileName = path.join(
        config.OUTPUT_DIR,
        "xero-contacts." + xero_tenant_id + "." + utils.getISOString() + ".json"
      );
      return fs.writeFile(fileName, JSON.stringify(response.data)).then(() => {
        logger.info("Wrote " + fileName);
        fileNames.push(fileName);
      });
    })
    .then(() => {
      return axios({
        method: "get",
        url: "Accounts",
        baseURL: config.XERO_API_ENDPOINT_V2,
        headers: {
          Authorization: "Bearer " + access_token,
          Accept: "application/json",
          "Xero-tenant-id": xero_tenant_id
        }
      });
    })
    .then(response => {
      var fileName = path.join(
        config.OUTPUT_DIR,
        "xero-accounts." + xero_tenant_id + "." + utils.getISOString() + ".json"
      );
      return fs.writeFile(fileName, JSON.stringify(response.data)).then(() => {
        logger.info("Wrote " + fileName);
        fileNames.push(fileName);
      });
    })
    .then(() => {
      res.status(201).json({ uris: fileNames });
    })
    .catch(err => {
      if (err.response) {
        next({ status: 400, errorCode: 2, data: err.response.data });
      } else if (err.request) {
        next({ status: 500, errorCode: 1, data: "Timeout" });
      } else if (err.message) {
        next({ status: 500, errorCode: 0, data: err.message });
      } else {
        next({ status: 500, errorCode: 0, data: JSON.stringify(err) });
      }
    });
});

module.exports = router;
