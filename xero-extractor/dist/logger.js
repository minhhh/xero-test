"use strict";

var config = require("./config");

var winston = require("winston");

var expressWinston = require("express-winston");

var hostname = require("os").hostname();

var ipAddress = require("ip").address();

var path = require("path");

var logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: {
    host: hostname,
    ip: ipAddress,
    timestamp: Date.now(),
    pid: process.pid
  },
  transports: [new winston.transports.File({
    filename: path.join(config.LOG_DIR, config.ERROR_LOG_FILENAME),
    level: "error"
  }), new winston.transports.File({
    filename: path.join(config.LOG_DIR, config.COMBINED_LOG_FILENAME)
  })]
});

if (config.NODE_ENV !== "production") {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
    level: "debug"
  }));
}

var expressWinstonLogTransports = [new winston.transports.File({
  filename: path.join(config.LOG_DIR, config.COMBINED_LOG_FILENAME)
})];
var expressWinstonErrorLogTransports = [new winston.transports.File({
  filename: path.join(config.LOG_DIR, config.ERROR_LOG_FILENAME)
})];

if (config.NODE_ENV !== "production") {
  expressWinstonLogTransports.push(new winston.transports.Console({
    format: winston.format.simple()
  }));
  expressWinstonErrorLogTransports.push(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

var expressWinstonLogger = expressWinston.logger({
  transports: expressWinstonLogTransports,
  format: winston.format.json(),
  baseMeta: {
    host: hostname,
    ip: ipAddress,
    timestamp: Date.now(),
    pid: process.pid
  },
  meta: true,
  msg: "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
  expressFormat: false,
  colorize: false,
  ignoreRoute: function ignoreRoute(req, res) {
    return false;
  }
});
var expressWinstonErrorLogger = expressWinston.errorLogger({
  transports: expressWinstonErrorLogTransports,
  format: winston.format.json()
});
module.exports = {
  logger: logger,
  expressWinstonLogger: expressWinstonLogger,
  expressWinstonErrorLogger: expressWinstonErrorLogger
};