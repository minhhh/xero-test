"use strict";

var config = require("./config");

var express = require("express");

var router = express.Router();

var logger = require("./logger").logger;

var axios = require("axios");

var utils = require("./utils");

var fs = require("fs").promises;

var path = require("path");

router.post("/", function (req, res, next) {
  var _req$body = req.body,
      access_token = _req$body.access_token,
      xero_tenant_id = _req$body.xero_tenant_id;
  var fileNames = []; // logger.debug("========req.body " + JSON.stringify (req.body));

  axios({
    method: "get",
    url: "Contacts",
    baseURL: config.XERO_API_ENDPOINT_V2,
    headers: {
      Authorization: "Bearer " + access_token,
      Accept: "application/json",
      "Xero-tenant-id": xero_tenant_id
    }
  }).then(function (response) {
    // logger.debug("========response " + JSON.stringify(response.data));
    // logger.debug("========time " + utils.getISOString());
    var fileName = path.join(config.OUTPUT_DIR, "xero-contacts." + xero_tenant_id + "." + utils.getISOString() + ".json");
    return fs.writeFile(fileName, JSON.stringify(response.data)).then(function () {
      logger.info("Wrote " + fileName);
      fileNames.push(fileName);
    });
  }).then(function () {
    return axios({
      method: "get",
      url: "Accounts",
      baseURL: config.XERO_API_ENDPOINT_V2,
      headers: {
        Authorization: "Bearer " + access_token,
        Accept: "application/json",
        "Xero-tenant-id": xero_tenant_id
      }
    });
  }).then(function (response) {
    var fileName = path.join(config.OUTPUT_DIR, "xero-accounts." + xero_tenant_id + "." + utils.getISOString() + ".json");
    return fs.writeFile(fileName, JSON.stringify(response.data)).then(function () {
      logger.info("Wrote " + fileName);
      fileNames.push(fileName);
    });
  }).then(function () {
    res.status(201).json({
      uris: fileNames
    });
  })["catch"](function (err) {
    if (err.response) {
      next({
        status: 400,
        errorCode: 2,
        data: err.response.data
      });
    } else if (err.request) {
      next({
        status: 500,
        errorCode: 1,
        data: "Timeout"
      });
    } else if (err.message) {
      next({
        status: 500,
        errorCode: 0,
        data: err.message
      });
    } else {
      next({
        status: 500,
        errorCode: 0,
        data: JSON.stringify(err)
      });
    }
  });
});
module.exports = router;