"use strict";

function getISOString() {
  return new Date().toISOString().replace(/-/g, "").replace(/\./g, "").replace(/:/g, "");
}

module.exports = {
  getISOString: getISOString
};