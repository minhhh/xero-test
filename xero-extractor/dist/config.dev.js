"use strict";

module.exports = {
  NODE_ENV: process.env.NODE_ENV || "development",
  PORT: process.env.PORT || 3000,
  CLIENT_ID: "FAAF9D836E2F49719878F25A7AC025B3",
  CLIENT_SECRET: "f6IboYxiOl5w5PlSXOjJfNtaM0YmCq_zb3JTeEsB9GcWku6U",
  XERO_API_ENDPOINT_V2: "https://api.xero.com/api.xro/2.0",
  LOG_DIR: "/tmp",
  COMBINED_LOG_FILENAME: "combined.log",
  ERROR_LOG_FILENAME: "error.log",
  OUTPUT_DIR: "/tmp"
};