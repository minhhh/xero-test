"use strict";

var config = require("./config");

var debug = require("debug")("web");

var express = require("express");

var createError = require("http-errors");

var path = require("path");

var bodyparser = require("body-parser");

var logger = require("./logger").logger;

var expressWinstonLogger = require("./logger").expressWinstonLogger;

var expressWinstonErrorLogger = require("./logger").expressWinstonErrorLogger;

var extractController = require("./ExtractController");

debug("Start creating app instance");
var app = express();
app.use(expressWinstonLogger);
app.use(bodyparser.urlencoded({
  extended: true
}));
app.use(bodyparser.json());
app.use("/extract", extractController);
app.get("/health", function (req, res) {
  res.send();
}); // catch 404 and forward to error handler

app.use(function (req, res, next) {
  next(createError(404));
});
app.use(expressWinstonErrorLogger); // error handler

app.use(function (err, req, res, next) {
  var status = err && err.status || 500;
  res.status(status || 500);
  res.json(err);
});
module.exports = app;