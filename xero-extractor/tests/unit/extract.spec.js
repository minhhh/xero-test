const config = require("./config");
const testRequest = require("supertest");
const axios = require("axios");
const path = require("path");
const app = require("../../src/server.js");
const fs = require("fs").promises;
import { expect } from "chai";

function getAccessToken() {
  var tokenFilename = path.join(__dirname, "tokens.json");
  return fs
    .readFile(tokenFilename)
    .then(res => {
      var tokens = JSON.parse(res);
      return tokens.refresh_token;
    })
    .then(res => {
      return axios({
        method: "post",
        url: config.XERO_TOKEN_ENDPOINT,
        headers: {
          Authorization:
            "Basic " +
            Buffer.from(config.CLIENT_ID + ":" + config.CLIENT_SECRET).toString(
              "base64"
            ),
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: "grant_type=refresh_token&refresh_token=" + res
      }).then(res => {
        return fs
          .writeFile(tokenFilename, JSON.stringify(res.data, null, 2))
          .then(() => {
            return res.data.access_token;
          });
      });
    });
}

describe("GET /extract", function() {
  it("Extract successfully", function() {
    return getAccessToken().then(res => {
      return testRequest(app)
        .post("/extract")
        .send({
          access_token: res,
          xero_tenant_id: config.XERO_TENANT_ID
        })
        .expect(201)
        .expect("Content-Type", /json/);
    });
  });

  it("Handle wrong access token error", function() {
    return testRequest(app)
      .post("/extract")
      .send({
        access_token: "wrong",
        xero_tenant_id: config.XERO_TENANT_ID
      })
      .expect(400)
      .expect("Content-Type", /json/)
      .then(res => {
        expect(res.body).to.exist;
        expect(res.body).to.include.all.keys("status", "errorCode", "data");
        expect(res.body.status).to.equal(400);
        expect(res.body.errorCode).to.equal(2);
      });
  });
});
